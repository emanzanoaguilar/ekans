import pygame, random, sys
from pygame.locals import *     #importam les llibreries de pygame
#print(pygame.font.get_fonts())

def main(): #Tota la funcio main es la encarregada de mostrar els botons de la pantalla
    fps = 60 #Establim la Freqüència de refrescat de la pantalla de mort
    size = [600, 600] #resolució
    bg = [0, 0, 0]

    screen = pygame.display.set_mode(size) #cream la pantalla

    button = pygame.Rect(100, 440, 100, 50)
                                                #pintam els dos botons
    button2 = pygame.Rect(400, 440, 100, 50)
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False

            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = event.pos                   #detectam la pulsacio dels botons
                if button.collidepoint(mouse_pos):      # Torna a começar el joc
                    pygame.display.update();
                    pygame.time.wait(250);
                    joc()
                if button2.collidepoint(mouse_pos):     #acaba el joc
                    sys.exit()
        screen.fill(bg)

        pygame.draw.rect(screen, [0, 250, 0], button) #posicio dels botons
        pygame.draw.rect(screen, [255, 0, 0], button2)
        #print(pygame.font.get_fonts()) Aixo va servir per comprovar les fonts

        #TEXTOS EN PANTALLA, COMPROVAM LA FONT. SI NO HI HA PRESS START 2P, USAM ARIAL

        if 'pressstart2p' in pygame.font.get_fonts():
            f = pygame.font.SysFont('Press Start 2P', 40)
        else:
            f = pygame.font.SysFont('Arial', 40)
        t = f.render('Play again?...' + str() + 'Insert Coin', True, (250, 250, 250));
        screen.blit(t, (75, 270));

        if 'pressstart2p' in pygame.font.get_fonts():
            f = pygame.font.SysFont('Press Start 2P', 20)
        else:
            f = pygame.font.SysFont('Arial', 20)
        t = f.render('Not Now...' + str(), True, (250, 250, 250));
        screen.blit(t, (405, 415));

        if 'pressstart2p' in pygame.font.get_fonts():
            f = pygame.font.SysFont('Press Start 2P', 20)
        else:
            f = pygame.font.SysFont('Arial', 20)
        t = f.render('Hell Yeah' + str(), True, (250, 250, 250));
        screen.blit(t, (105, 415));
        pygame.display.update()
        clock.tick(fps)

    pygame.quit()
    sys.exit
def colisio(x1, x2, y1, y2, w1, w2, h1, h2): #Comprovam i establim les cordenades de la serp
    if x1 + w1 > x2 and x1 < x2 + w2 and y1 + h1 > y2 and y1 < y2 + h2:
        return True
    else:
        return False

def mort(pantalla, punts): #Definim una funció mort() per establir quan acaba el joc
    a = open('score.txt','r')
    record = int(a.readline())
    a.close()
    if punts == 6:
        print("omkegalol, ( ͡° ͜ʖ ͡°)") #easter egg
    if punts > record:
        a = open('score.txt', 'w') #llegim la puntuacio maxima actual del arxiu score.txt
        a.write(str(punts))
        a.close()
        if 'pressstart2p' in pygame.font.get_fonts(): #en cas de superar el record
            f = pygame.font.SysFont('Press Start 2P', 17)
            t = f.render('Has superat el record amb ' + str(punts) + ' punts', True, (250, 250, 250));
            pantalla.blit(t, (10, 270));
            pygame.display.update();
            pygame.time.wait(3000);
            main()
        else:
            f = pygame.font.SysFont('Arial', 25);
            t = f.render('Has superat el record amb ' + str(punts) + ' punts', True, (250, 250, 250));
            pantalla.blit(t, (30, 270));
            pygame.display.update();
            pygame.time.wait(3000);
            main()
    else:
        if 'pressstart2p' in pygame.font.get_fonts(): #en cas de no superar-lo
            f = pygame.font.SysFont('Press Start 2P', 15)
            t = f.render('Has mort amb una puntuacio de: ' + str(punts)+' punts', True, (250, 250, 250));
            pantalla.blit(t, (10, 270));
            f = pygame.font.SysFont('Press Start 2P', 15)
            t = f.render('El record actual es de ' + str(record) + ' punts', True, (250, 250, 250));
            pantalla.blit(t, (70, 350));
            pygame.display.update();
            pygame.time.wait(3000);
            main()
        else:
            f = pygame.font.SysFont('Arial', 25);
            t = f.render('Has mort amb una puntuacio de: ' + str(punts)+' punts', True, (250, 250, 250));
            pantalla.blit(t, (30, 270));
            t = f.render('El record actual es de ' + str(record) + ' punts', True, (250, 250, 250));
            pantalla.blit(t, (30, 327));
            pygame.display.update();
            pygame.time.wait(2000);
            main()
def joc():
    #Cream variables per als objectes en pantalles
    xs = [290, 290, 290, 290, 290];
    ys = [290, 270, 250, 230, 210];
    direccio = 0; #aquesta variable estableix la direcció a la que mira la serp quan comença el joc
    punts = 0;
    poma_pos = (random.randint(100, 500), random.randint(100, 500)); #Feim que la poma aparegui en una posició aleatoria
    pygame.init();
    p = pygame.display.set_mode((600, 600)); #Definim la resolucio de la pantalla
    pygame.display.set_caption('Snake');
    poma_img = pygame.Surface((10, 10));#Li donam mides a la poma
    poma_img.fill((0, 255, 0));#Donam el color verd a la poma
    img = pygame.Surface((20, 20));#Cream les dimensions de cada segment de la serp
    img.fill((255, 0, 0));
    f = pygame.font.SysFont('Arial', 20); #definim la f0nt de la lletra per els missatges en pantalla
    global clock
    clock = pygame.time.Clock() #velocitat del joc fora valor
    while True:
        clock.tick(15) #velocitat del joc
        for e in pygame.event.get(): #Detectam les tecles que pitja el jugador per controlar la serp
            if e.type == QUIT:
                sys.exit(0)
            elif e.type == KEYDOWN:
                if e.key == K_UP and direccio != 0:
                    direccio = 2
                elif e.key == K_DOWN and direccio != 2:
                    direccio = 0
                elif e.key == K_LEFT and direccio != 1:
                    direccio = 3
                elif e.key == K_RIGHT and direccio != 3:
                    direccio = 1
                elif e.key == K_SPACE and direccio !=0:
                    clock.tick(15)
                elif e.key == K_SPACE and direccio !=0:
                    clock.tick(15)

        i = len(xs) - 1
        while i >= 2: #Si la serp es toca el cos amb el cap, perds el joc
            if colisio(xs[0], xs[i], ys[0], ys[i], 20, 20, 20, 20): mort(p, punts)
            i -= 1
        #Comprovam si la serp es menja una poma, si ho fa augmentam la seva mida
        if colisio(xs[0], poma_pos[0], ys[0], poma_pos[1], 20, 10, 20, 10): punts += 1;xs.append(700);ys.append(
            700);poma_pos = (random.randint(100, 500), random.randint(100, 500))
        if xs[0] < 0 or xs[0] > 580 or ys[0] < 0 or ys[0] > 580: mort(p, punts)
        i = len(xs) - 1
        while i >= 1: #Feim que la serp es desplaci sense haver de mantenir pitjada la fletxa
            xs[i] = xs[i - 1];
            ys[i] = ys[i - 1];
            i -= 1
        if direccio == 0:
            ys[0] += 20
        elif direccio == 1:
            xs[0] += 20
        elif direccio == 2:
            ys[0] -= 20
        elif direccio == 3:
            xs[0] -= 20
        p.fill((0, 0, 0))
        for i in range(0, len(xs)): #Renderitzam la serp en pantalla
            p.blit(img, (xs[i], ys[i]))
        p.blit(poma_img, poma_pos);
        if 'pressstart2p' in pygame.font.get_fonts():
            f = pygame.font.SysFont('Press Start 2P', 30)
        else:
            f = pygame.font.SysFont('Arial', 20)
        t = f.render("Punts: "+str(punts), True, (250, 250, 250));
        p.blit(t, (10, 10));
        pygame.display.update()
joc()
